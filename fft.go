package main

import (
	"math"
	"fmt"
	"math/cmplx"
)

func PadData(data []float64) []float64 {
	lenLog := math.Log2(float64(len(data)))
	fmt.Println(float64(len(data)))
	fmt.Println(lenLog)
	if math.Mod(lenLog, 1) == 0 {
		return data
	}
	newLen := int(math.Exp2(math.Ceil(lenLog)))
	for len(data) < newLen {
		data = append(data, 0)
	}
	return data
}

func getOddElems(data []float64) []float64 {
	var garbo []float64
	for i, v := range data {
		if i % 2 != 0 {
			garbo = append(garbo, v)
		}
	}
	return garbo
}

func getEvenElems(data []float64) []float64 {
	var garbo []float64
	for i, v := range data {
		if i % 2 == 0 {
			garbo = append(garbo, v)
		}
	}
	return garbo
}

func _fastFourierTransform(data []float64, length int, stride int) []complex128 {
	if length == 1 {
		return []complex128{complex(data[0], 0)}
	}
	var X_K []complex128
	var X_KN_2 []complex128
	x_k := _fastFourierTransform(getEvenElems(data), length/2, 2*stride)
	x_kn_2 := _fastFourierTransform(getOddElems(data), length/2, 2*stride)
	for k := 0; k < length/2; k++ {
		omega_k := cmplx.Exp(complex(math.Pi, 0)*-2i/complex(float64(length), 0)*complex(float64(k), 0)) * x_kn_2[k]
		X_K = append(X_K, x_k[k]+omega_k)
		X_KN_2 = append(X_KN_2, x_k[k]-omega_k)
	}
	return append(X_K, X_KN_2...)
}

func FastFourierTransform(data []float64) []complex128 {
	data = PadData(data)
	fftData := _fastFourierTransform(data, len(data), 1)
	for i, d := range fftData {
		fftData[i] = d / complex(float64(len(fftData)), 0)
	}
	return fftData
}
