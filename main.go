package main

import (
	"math"
	"math/cmplx"
)

func Input(
	x float64,
) float64 {
	return math.Cos(x) + math.Sin(3*x)
}

func CalculateDelta(
	a, b []complex128,
) []float64 {
	if len(a) != len(b) {
		panic("FUCK")
	}
	delta := make([]float64, len(a))

	for i := 0; i < len(a); i++ {
		delta[i] = cmplx.Abs(a[i]) - cmplx.Abs(b[i])
	}

	return delta
}

func main() {
	// setup
	sampleCount := 2047
	samples := PadData(GenerateSamplesFromContiniousFunc(sampleCount, 2.0*math.Pi, Input))
	mp := Multiplot{
		totalCols: 1,
		totalRows: 7,
	}

	// DFT
	coefficients := DiscreteFourierTransform(samples)
	inverse := InverseDiscreteFourierTransform(coefficients)

	p1 := MultiplotEntry{
		posX: 0,
		posY: 0,
		content: PlotDataSeries(samples,
			"Source signal samples",
			"sample #",
			"math.Cos(x) + math.Sin(3*x)",
			"f(x)"),
	}
	mp = AddPlotToMultiplot(mp, p1)

	p2 := MultiplotEntry{
		posX: 1,
		posY: 0,
		content: PlotAbsoluteValues(coefficients,
			"Amplitude spectrum(cosines)",
			"frequency",
			"normalizing factor / 2",
			"DFT"),
	}
	mp = AddPlotToMultiplot(mp, p2)

	p21 := MultiplotEntry{
		posX: 2,
		posY: 0,
		content: PlotArgumentValues(coefficients,
			"Phase spectrum",
			"frequency",
			"Pi",
			"DFT"),
	}
	mp = AddPlotToMultiplot(mp, p21)

	p3 := MultiplotEntry{
		posX: 3,
		posY: 0,
		content: PlotRealValues(inverse,
			"IDFT(RE)",
			"sample #",
			"math.Cos(x) + math.Sin(3*x)",
			"IDFT: f(x)"),
	}
	mp = AddPlotToMultiplot(mp, p3)

	// FFT
	fftCoefficients := FastFourierTransform(samples)
	fftInverse := InverseDiscreteFourierTransform(fftCoefficients)

	p4 := MultiplotEntry{
		posX: 4,
		posY: 0,
		content: PlotAbsoluteValues(fftCoefficients,
			"Amplitude spectre(cosines)",
			"frequency",
			"normalizing factor / 2",
			"FFT"),
	}
	mp = AddPlotToMultiplot(mp, p4)

	p41 := MultiplotEntry{
		posX: 5,
		posY: 0,
		content: PlotArgumentValues(fftCoefficients,
			"Phase spectrum",
			"frequency",
			"Pi",
			"FFT"),
	}
	mp = AddPlotToMultiplot(mp, p41)

	p5 := MultiplotEntry{
		posX: 6,
		posY: 0,
		content: PlotRealValues(fftInverse,
			"IFFT",
			"sample #",
			"Reconstructed",
			"delta"),
	}
	mp = AddPlotToMultiplot(mp, p5)

	PlotMultiPlot(mp)
}
