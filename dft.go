package main

import (
	"math"
	"math/cmplx"
)

func GenerateSamplesFromContiniousFunc(
	sampleRate int,
	capturePeriod float64,
	funcGeneration func(float64) float64,
) []float64 {
	/// sampeRate = # of samples / capture period
	angularFrequency := capturePeriod / float64(sampleRate)
	var samples []float64
	for i := 0.0; i < capturePeriod; i += angularFrequency {
		samples = append(samples, funcGeneration(i))
	}

	return samples
}

func ApplyPower(
	data []complex128,
	power func(j, k, n int) float64,
	normalize func(c, n complex128) complex128,
) []complex128 {
	var output []complex128
	n := len(data)
	for k := 0; k < n; k++ {
		var cVal complex128 = 0
		for j := 0; j < n; j++ {
			cVal += data[j] * cmplx.Exp(complex(0.0, power(j, k, n)))
		}
		output = append(output, normalize(cVal, complex(float64(n), 0.0)))
	}

	return output
}

func GetPower(
	j, k, n int,
) float64 {
	return -float64(j) *
		float64(k) *
		math.Pi * 2.0 /
		float64(n)
}

func GetInversePower(
	j, k, n int,
) float64 {
	return float64(j) *
		float64(k) *
		math.Pi * 2.0 /
		float64(n)
}

func DiscreteFourierTransform(
	samples []float64,
) []complex128 {
	var complexes []complex128
	for _, v := range samples {
		complexes = append(complexes, complex(v, 0))
	}
	return ApplyPower(complexes, GetPower, func(c, n complex128) complex128 { return c })
}

func InverseDiscreteFourierTransform(
	fourierCoefficients []complex128,
) []complex128 {
	return ApplyPower(fourierCoefficients, GetInversePower, func(c, n complex128) complex128 { return c / n })
}
