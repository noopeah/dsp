package main

import (
	"fmt"
	"math"
	"os"
	"math/cmplx"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/plotutil"
	"gonum.org/v1/plot/vg"
	"gonum.org/v1/plot/vg/draw"
	"gonum.org/v1/plot/vg/vgimg"
)

type MultiplotEntry struct {
	posX    int
	posY    int
	content *plot.Plot
}

type Multiplot struct {
	plots                []MultiplotEntry
	totalCols, totalRows int
}

func AddPlotToMultiplot(
	mp Multiplot,
	me MultiplotEntry,
) Multiplot {
	mp.plots = append(mp.plots, me)
	fmt.Println(mp.plots)
	return mp
}

func PlotMultiPlot(
	mp Multiplot,
) {
	img := vgimg.New(vg.Points(2560), vg.Points(1440))
	dc := draw.New(img)

	t := draw.Tiles{
		Rows: mp.totalRows,
		Cols: mp.totalCols,
	}

	plots := make([][]*plot.Plot, mp.totalRows)
	for j := 0; j < mp.totalRows; j++ {
		plots[j] = make([]*plot.Plot, mp.totalCols)
	}

	for _, value := range mp.plots {
		plots[value.posX][value.posY] = value.content
	}

	canvases := plot.Align(plots, t, dc)
	for j := 0; j < mp.totalRows; j++ {
		for i := 0; i < mp.totalCols; i++ {
			if plots[j][i] != nil {
				plots[j][i].Draw(canvases[j][i])
			}
		}
	}

	w, err := os.Create("multiplot.png")
	if err != nil {
		panic(err)
	}

	png := vgimg.PngCanvas{Canvas: img}
	if _, err := png.WriteTo(w); err != nil {
		panic(err)
	}
}

func PlotAbsoluteValues(
	vals []complex128,
	name, xName, yName, series string,
) *plot.Plot {
	var targetValues []float64
	for _, value := range vals {
		targetValues = append(targetValues, math.Hypot(real(value), imag(value)))
	}

	return PlotDataSeries(targetValues, name, xName, yName, series)
}

func PlotRealValues(
	vals []complex128,
	name, xName, yName, series string,
) *plot.Plot {
	var targetValues []float64
	for _, value := range vals {
		targetValues = append(targetValues, real(value))
	}

	return PlotDataSeries(targetValues, name, xName, yName, series)
}

func PlotArgumentValues(
	vals []complex128,
	name, xName, yName, series string,
) *plot.Plot {
	var targetValues []float64
	for _, value := range vals {
		targetValues = append(targetValues, cmplx.Phase(value))
	}

	return PlotDataSeries(targetValues, name, xName, yName, series)
}

func PlotImaginaryValues(
	vals []complex128,
	name, xName, yName, series string,
) *plot.Plot {
	var targetValues []float64
	for _, value := range vals {
		targetValues = append(targetValues, imag(value))
	}

	return PlotDataSeries(targetValues, name, xName, yName, series)
}

func PlotDataSeries(
	vals []float64,
	name, xName, yName, series string,
) *plot.Plot {
	sampleIdx := 0
	p := plot.New()
	p.Title.Text = name
	p.X.Label.Text = xName
	p.Y.Label.Text = yName

	points := make(plotter.XYs, len(vals))
	for i := range points {
		points[i].X = float64(sampleIdx)
		points[i].Y = vals[sampleIdx]
		sampleIdx++
	}

	if err := plotutil.AddLinePoints(p, series, points); err != nil {
		panic(err)
	}

	return p
}
